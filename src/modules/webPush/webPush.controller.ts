
import {Request,Response} from 'express'
import { get } from 'lodash'
import getOptions from '../../untils/getOptions'
import tryCatch from '../../untils/tryCatch'
import { createWebPushService, deleteWebPushService, getWebPushExistsService, getWebPushService } from './webPush.service';
import web_push from 'web-push';
export const getWebPushKeyController = tryCatch(async(req:Request,res:Response) => {
    res.send(process.env.PUBLICKEY_WEBPUSH);
});
export const subscribeController = tryCatch(async(req:Request,res:Response) => {
    const {subscription,userId} = req.body;
    const subscriptionExist = await getWebPushExistsService({endpoint : get(subscription,'endpoint'),userId});
    if(subscriptionExist){
        deleteWebPushService(get(subscriptionExist,'_id'));
    }
    await createWebPushService({subscription,userId});
    res.status(201).json({ message: 'Subscription saved' });
});

export const pushNotificationController = tryCatch(async(req:Request,res:Response) => {
    const {title,data} = req.body;
    // Payload notification
    const notificationPayload = JSON.stringify({ title: 'Push Notification', body: title });

    // Filter Subscriptions to send notification
    const subscriptions = await getWebPushService({});
    if(subscriptions.length === 0){
        return res.status(200).json({ message: 'No subscription found' });
    }
    // Send notification
    Promise.all(subscriptions.map(({subscription}:any) =>
    {        
        if (subscription) {
            return web_push.sendNotification(subscription, notificationPayload);
        }
        return Promise.resolve(); // Return a resolved Promise if 'subscription' is null or undefined
    }
  ))
    .then(() => res.status(200).json({ message: 'Notification sent' }))
    .catch(error => {
      console.error('Error sending push notification:', error);
      res.status(500).json({ error: 'Notification failed to send' });
    });
    res.status(201).json({ message: 'Subscription saved' });
});

export const unSubscribeController = tryCatch(async(req:Request,res:Response) => {
    const {subscription,userId} = req.body;
    const subscriptionExist = await getWebPushExistsService({endpoint : get(subscription,'endpoint'),userId});
    if(subscriptionExist){
        deleteWebPushService(get(subscriptionExist,'_id'));
    }
    res.status(201).json({ message: 'Subscription saved' });
});