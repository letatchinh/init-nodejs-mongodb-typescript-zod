import mongoose, { Schema, Types } from "mongoose";
import paginate from 'mongoose-paginate-v2';

 interface Subscription {
    endpoint: string;
    expirationTime?: Date;
    keys: {
        auth: string;
        p256dh: string;
    };
}

 interface WebPushDocument extends mongoose.Document {
    subscription: Subscription;
    userId:  Schema.Types.ObjectId;
}

const WebPushSchema = new Schema<WebPushDocument>({
    subscription: { type: Object, require: true },
    userId: { type:  Schema.Types.ObjectId, require: true, ref: "UserSchema" },
}, {
    timestamps: true,
});
WebPushSchema.plugin(paginate)
const WebPushCollection = mongoose.model<WebPushDocument, mongoose.PaginateModel<WebPushDocument>>('WebPush', WebPushSchema, 'WebPush');
export default WebPushCollection;