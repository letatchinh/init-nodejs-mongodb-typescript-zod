import { Express } from 'express';
import Authentication from '../../middleware/Authentication';
import { getWebPushKeyController, pushNotificationController, subscribeController, unSubscribeController } from './webPush.controller';
const routes = (app:Express) => {
    app.get("/api/v1/webPush-key",Authentication,getWebPushKeyController);
    app.post("/api/v1/webPush-subscribe",Authentication,subscribeController);
    app.post("/api/v1/webPush-unSubscribe",Authentication,unSubscribeController);
    app.post("/api/v1/webPush-push-notification",Authentication,pushNotificationController);
}
export default routes