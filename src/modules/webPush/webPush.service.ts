import { OptionsPaginate } from "../../untils/getOptions";
import WebPushCollection from "./webPush.model";

export const createWebPushService = async(input:any) => {
    try {
        return await WebPushCollection.create(input)
    } catch (error:any) {
        throw new Error(error)
    }
}
export const getWebPushService = async(query:any) => {
    try {
        return await WebPushCollection.find(query)
    } catch (error:any) {
        throw new Error(error)
    }
}

export const getWebPushExistsService = async(query:any) => {
    try {
        const {endpoint,userId} = query;
        return await WebPushCollection.find({
            subscription: {
                endpoint,
            },
            userId,
        });
    } catch (error:any) {
        throw new Error(error)
    }
}
export async function deleteWebPushService(id : any) {
    try {
         await WebPushCollection.findByIdAndDelete(id);
    } catch (error : any) {
        throw new Error(error)
    }
}