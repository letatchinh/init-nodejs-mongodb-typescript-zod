import { Express } from 'express';
import { createUser, login } from './user.controller';
import { getUserByTokenService } from './user.service';
const routes = (app:Express) => {
    app.post("/api/v1/user/register",createUser);
    app.post("/api/v1/user/login",login);
    app.get("/api/v1/user/getProfile/:token",getUserByTokenService);
}
export default routes