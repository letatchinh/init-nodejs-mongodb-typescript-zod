import mongoose from "mongoose";
export interface UserDocument extends mongoose.Document{
    username: string;
    password: string;
    fullName: string;
    email?: string;
    phoneNumber?: string;
    accessKey: string[];
    createdAt:Date;
    updatedAt:Date;
}
const UserSchema = new mongoose.Schema({
    username : {type : String,require : true},
    password : {type : String,require : true},
    fullName : String,
    email : String,
    phoneNumber : String,
    accessKey : {type : Array,default : []},
},
{
    timestamps : true,
})
const User = mongoose.model('UserSchema', UserSchema,'UserSchema');
export default User
