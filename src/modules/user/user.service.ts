import { get } from "lodash";
import { Schema } from "mongoose";
import { handleCompare, handleHash } from "../../untils/bcryptComon";
import { InvalidError, NotFoundError } from "../../untils/ErrorBase";
import { verifyTokenService } from "../../untils/jwt";
import User from "./user";

export async function getUserByIdService(id: Schema.Types.ObjectId) {
    try {
        return await User.findById(id);
    } catch (error: any) {
        throw new Error(error)
    }
}
export async function getUserByTokenService(token: string) {
    try {
        const user = verifyTokenService(token);
        console.log(verifyTokenService,'verifyTokenService');
        return await User.findById(get(user,'_id'));
    } catch (error: any) {
        throw new Error(error)
    }
}
export async function createUserService(input: any) {
    try {
        const { password } = input
        const hashPassword = handleHash(password)
        return await User.create({...input,password:hashPassword});
    } catch (error: any) {
        throw new Error(error)
    }
}
export async function getAllUsersCreateNotificationService(query: any,notificationId:Schema.Types.ObjectId) {
    try {
        return await User.aggregate()
        .addFields({
            notificationId
        });
    } catch (error: any) {
        throw new Error(error)
    }
}
export async function loginUserService(input: any) {
    try {
        const { username,password } = input
        if(!username || !password) {
            throw new InvalidError("Không được để trống tài khoản hoặc mật khẩu")
        }
        const user = await User.findOne({username})
        if(!user){
            throw new NotFoundError("Sai tài khoản hoặc mật khẩu")
        }
        const isEqualPassword = handleCompare(password,user.password || "")
        if(!isEqualPassword){
            throw new NotFoundError("Sai tài khoản hoặc mật khẩu")
        }
        return user
    } catch (error: any) {
        throw new Error(error)
    }
}
