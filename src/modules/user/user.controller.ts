
import { Request, Response } from 'express'
import jwt from 'jsonwebtoken'
import { AuthenticateError } from '../../untils/ErrorBase'
import { signToken } from '../../untils/jwt'
import tryCatch from '../../untils/tryCatch'
import { createUserService, getUserByIdService, getUserByTokenService, loginUserService } from './user.service'
export const createUser = tryCatch(async (req: Request, res: Response) => {

    const user = await createUserService(req.body)
    res.send(user)
});

export const getProfileByTokenController = tryCatch(async (req: Request, res: Response) => {
    const { token } = req.params;
    const user = await getUserByTokenService(token);
    res.send(user)
});
export const login = tryCatch(async (req: Request, res: Response) => {
    const user = await loginUserService(req.body);
    
    const { _id, username, fullName } = user;
    const accessToken = signToken({ profile: { _id, username, fullName } });
    console.log(accessToken,'accessToken');
    
    res.send(
        {
            profile: { _id, username, fullName },
            token: accessToken
        }
    )
})