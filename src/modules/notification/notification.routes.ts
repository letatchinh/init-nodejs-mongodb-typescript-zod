import { Express } from 'express';
import Authentication from '../../middleware/Authentication';
import { createNotification, deleteNotification, getNotification, getNotifications, updateNotification } from './notification.controller';
const routes = (app:Express) => {
    app.post("/api/v1/notification",
    // Authentication,
    createNotification
    );
    app.get("/api/v1/notification",
    Authentication,
    getNotifications
    );
    app.get("/api/v1/notification/:id",
    Authentication,
    getNotification
    );

    app.put("/api/v1/notification/:id",
    Authentication,
    updateNotification
    );
    app.delete("/api/v1/notification/:id",
    Authentication,
    deleteNotification
    );
}
export default routes