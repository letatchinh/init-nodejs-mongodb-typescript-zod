import { Types } from "mongoose";
import { OptionsPaginate } from "../../untils/getOptions";
import Notification, { NotificationDocument } from "./notification";

export async function createNotificationService(input:NotificationDocument) {
    try {
        return await Notification.create(input);
    } catch (error : any) {
        throw new Error(error)
    }
}

export async function getNotificationsService(query : any = {},options:OptionsPaginate) {
    try {
        return await Notification.aggregatePaginate(Notification.aggregate(),options)
    } catch (error : any) {
        throw new Error(error)
    }
}
export async function getNotificationService(id : any) {
    try {
        const todo = await Notification.findById(id);
        return todo
    } catch (error : any) {
        throw new Error(error)
    }
}
export async function updateNotificationService(id : any,params : any) {
    try {
        const todo = await Notification.findByIdAndUpdate(id,params);
        return todo
    } catch (error : any) {
        throw new Error(error)
    }
}
export async function deleteNotificationService(id : any) {
    try {
        const todo = await Notification.findByIdAndDelete(id);
        return todo
    } catch (error : any) {
        throw new Error(error)
    }
}

export async function getNotificationsShareService(query : any = {},options:OptionsPaginate) {
    try {
        const {roles} = query;
        let rolesArray:any = [];
        const todoIds = roles?.map((item:string) => {
            const splitRole = item.split(':');
            const role = splitRole[0];
            const resource = splitRole[1];
            rolesArray.push({id:resource,role})
            return new Types.ObjectId(resource)
        });
        return await Notification.aggregatePaginate(Notification.aggregate()
          .match({_id : {$in : todoIds}})
          .addFields({
            idth: { $toString: '$_id' },
            role: rolesArray // Ass
        })
        .addFields({
            role : {
                $arrayElemAt: [{
                    $filter : {
                        input : '$role',
                        as : 'item',
                        cond: {
                            $eq :['$$item.id','$idth']
                        },
                        limit : 1
                    }
                },0]
            },
        })
        .addFields({
            role : '$role.role'
        })
        ,options)
    } catch (error : any) {
        throw new Error(error)
    }
}