
import { Request, Response } from 'express'
import getOptions from '../../untils/getOptions'
import tryCatch from '../../untils/tryCatch'
import { createNotificationService, deleteNotificationService, getNotificationService, getNotificationsService, updateNotificationService } from './notification.service'
export const createNotification = tryCatch(async(req:Request,res:Response) => {
    const notification = await createNotificationService(req.body);
    res.send(notification)
})
export const getNotifications = tryCatch(async(req:Request,res:Response) => {
    const options = getOptions(req.query);
    options.populate = {path : 'userId',select : 'fullName email phoneNumber'}
    const {} = req.query
    const notifications = await getNotificationsService({},options);
    
    res.send(notifications)
})
export const getNotification = tryCatch(async(req:Request,res:Response) => {
    const {id} = req.params;
    const notification = await getNotificationService(id);
    res.send(notification)
})
export const updateNotification = tryCatch(async(req:Request,res:Response) => {
    const {id} = req.params;
    const notification = await updateNotificationService(id,req.body);
    res.send(notification)
})
export const deleteNotification = tryCatch(async(req:Request,res:Response) => {
    const {id} = req.params;
    const notification = await deleteNotificationService(id);
    res.send(notification)
});
