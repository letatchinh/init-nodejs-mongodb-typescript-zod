import mongoose from "mongoose";
import mongooseAggregatePaginate from "mongoose-aggregate-paginate-v2";
import mongoosePaginate from 'mongoose-paginate-v2';
import CombinedPaginateModel from "../../untils/ComparePaginate";
import { insertManyNotificationUserService } from "../notificationUser/notificationUser.service";
import { TYPE_NOTIFICATION } from "./defaultValue";

export interface NotificationDocument extends mongoose.Document {
    title: string;
    type:string;
    data?:any;
}


const NotificationSchema = new mongoose.Schema({
    title: { type: String, require: true },
    type: { type: String, enum : Object.values(TYPE_NOTIFICATION), require: true ,default : TYPE_NOTIFICATION.CREATE_TODO },
    data: { type: Object },
}, {
    timestamps: true,
});
  NotificationSchema.plugin(mongoosePaginate);
  NotificationSchema.plugin(mongooseAggregatePaginate);

  NotificationSchema.pre('save',async function (this,next:any) {
    if(!this.isNew){
      next()
    }
    else{
      const notificationId = new mongoose.Types.ObjectId(this._id) as unknown as mongoose.Schema.Types.ObjectId;
      await insertManyNotificationUserService({},notificationId)
    }

  })
  const Notification = mongoose.model<NotificationDocument,CombinedPaginateModel<NotificationDocument>>('Notification', NotificationSchema,'Notification');
  export default Notification;