import { NextFunction } from 'express';
import mongoose, { Document, Model } from 'mongoose';
import { optionsAutoIncrement } from '../../untils/AutoIncrement';

interface CounterDocument extends Document {
  seq: number;
  modelName:string;
}
interface paramsAutoIncrement extends optionsAutoIncrement {
    doc: any,
    next: NextFunction,
}
const counterSchema = new mongoose.Schema<CounterDocument>({
  modelName: { type: String, required: true },
  seq: { type: Number, default: 0 },
});

counterSchema.index({ modelName: 1, seq: 1 }, { unique: true });

const counterModel: Model<CounterDocument> = mongoose.model('counter', counterSchema,'counter');

const autoIncrementModelID = async function(
    paramsAutoIncrement : paramsAutoIncrement
): Promise<void> {
    const{ modelName,inc_field,doc,next,start_seq = 0,prefix=''} = paramsAutoIncrement
  try {
    const counter = await counterModel.findOneAndUpdate(
        {modelName},
      { $inc: { seq: 1 } },
      { new: true, upsert: true }
    );

    if (!counter) {
      throw new Error('Counter not found');
    }

    doc[inc_field] =`${prefix}${counter.seq + start_seq}`;
    next();
  } catch (error:any) {
    
    next(error);
  }
};

export default autoIncrementModelID;