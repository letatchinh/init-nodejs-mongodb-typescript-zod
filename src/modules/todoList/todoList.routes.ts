import {Express} from 'express';
import { CORE_ACTION } from '../../core/permission';
import Authentication from '../../middleware/Authentication';
import Authorization from '../../middleware/Authorization';
import validate from '../../middleware/validateResource';
import { addRolesTodoListValidate, removeRolesTodoListValidate } from '../../validate/todoList.schema';
import { addRolesForUser, createTodoList, getTodoList, getTodoLists, getTodoListShare, removeRolesForUser, removeTodoForCasbin, updateTodoList } from './todoList.controller';
const routes = (app:Express) => {
    app.post("/api/v1/todoList",
    Authentication,
    // validate(TodoListValidate),
    createTodoList
    );
    app.get("/api/v1/todoList",
    // Authentication,
    getTodoLists
    );
    app.get("/api/v1/todoList/share",
    Authentication,
    getTodoListShare
    );
    app.get("/api/v1/todoList/:id",
    Authorization([
        CORE_ACTION.read.value,
    ]),
    getTodoList
    );

    app.put("/api/v1/todoList/:id",
    Authorization([
        CORE_ACTION.update.value,
    ]),
    updateTodoList
    );
    app.delete("/api/v1/todoList/:id",
    Authorization([
        CORE_ACTION.delete.value,
    ]),
    removeTodoForCasbin
    );
    app.post(
        "/api/v1/todoList/addRoles",
        Authorization([
            CORE_ACTION.admin.value,
        ]),
        validate(addRolesTodoListValidate),
        addRolesForUser,
    )
    app.post(
        "/api/v1/todoList/removeRole",
        Authorization([
            CORE_ACTION.admin.value,
        ]),
        validate(removeRolesTodoListValidate),
        removeRolesForUser,
    )
}
export default routes