import mongoose, { Schema } from "mongoose";
import mongoosePaginate from 'mongoose-paginate-v2';
import mongooseAggregatePaginate from "mongoose-aggregate-paginate-v2";
import AutoIncrement from "../../untils/AutoIncrement";
import CombinedPaginateModel from "../../untils/ComparePaginate";
import { STATUS_TODOLIST } from "./defaultValue";
import { get, keys } from "lodash";
import casbin from "../../untils/casbin";
import { CORE_POLICIES } from "../../core/permission";

export interface TodoListDocument extends mongoose.Document {
    title: string;
    desc?: string;
    isImportant: boolean;
    createdAt: Date;
    tagIds: Schema.Types.ObjectId[];
    updatedAt: Date;
    todoSeq: string;
    status:string;

}


const TodoListSchema = new mongoose.Schema({
    title: { type: String, require: true },
    desc: { type: String },
    isImportant: { type: Boolean, require: true, default: false },
    todoSeq : {type:String, require: true, unique : true},
    userId:{type: Schema.Types.ObjectId,ref: 'UserSchema'},
    tagIds : [{type: Schema.Types.ObjectId,default : []}],
    status : {type : String,enum : keys(STATUS_TODOLIST),default:STATUS_TODOLIST.NEW},
}, {
    timestamps: true,
});
  TodoListSchema.plugin(AutoIncrement,{inc_field: 'todoSeq', modelName: 'todoList',start_seq : 10000})
  TodoListSchema.plugin(mongoosePaginate);
  TodoListSchema.plugin(mongooseAggregatePaginate);
  TodoListSchema.pre('save', async function (this, next:any) {
    try {
        if(this){
          const {userId,_id} = this
            await casbin.createGroupPolicyTodoList(String(_id));
            await casbin.addRoleForUser(String(userId),CORE_POLICIES.OWNER,String(_id));
        }
      next();
    } catch (error:any) {
        console.log(error);
        
      next(error);
    }
  });
  const TodoList = mongoose.model<TodoListDocument,CombinedPaginateModel<TodoListDocument>>('TodoList', TodoListSchema,'TodoList');
  export default TodoList;