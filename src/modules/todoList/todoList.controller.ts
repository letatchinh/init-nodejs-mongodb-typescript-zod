
import {Request,Response} from 'express'
import casbin from '../../untils/casbin'
import { addRoleUser } from '../../untils/casbin/interface'
import getOptions from '../../untils/getOptions'
import tryCatch from '../../untils/tryCatch'
import { createTodo, getTodosService,getTodoService, updateTodoService, deleteTodoService, getTodosShareService } from './todoList.service'
export const createTodoList = tryCatch(async(req:Request,res:Response) => {
    const todo = await createTodo(req.body);
    res.send(todo)
})
export const getTodoLists = tryCatch(async(req:Request,res:Response) => {
    const options = getOptions(req.query);
    options.populate = {path : 'userId',select : 'fullName email phoneNumber'}
    const {} = req.query
    const todos = await getTodosService({},options);
    
    res.send(todos)
})
export const getTodoList = tryCatch(async(req:Request,res:Response) => {
    const {id} = req.params;
    const todo = await getTodoService(id);
    res.send(todo)
})
export const updateTodoList = tryCatch(async(req:Request,res:Response) => {
    const {id} = req.params;
    const todo = await updateTodoService(id,req.body);
    res.send(todo)
})
export const deleteTodoList = tryCatch(async(req:Request,res:Response) => {
    const {id} = req.params;
    const todo = await deleteTodoService(id);
    res.send(todo)
});
export const addRolesForUser = tryCatch(async(req:Request,res:Response) => {
    const promises = req.body?.map((item:addRoleUser) =>{
        const {userId,role,todoId} = item
        return  casbin.addRoleForUser(String(userId),role,String(todoId))
    });
    await Promise.all(promises);
    res.send({status : true})
});
export const removeRolesForUser = tryCatch(async(req:Request,res:Response) => {
    const {userId,todoId} = req.body;
        await  casbin.removeRoleForUser(String(userId),String(todoId));
        res.send({status : true})
});
export const removeTodoForCasbin = tryCatch(async(req:Request,res:Response) => {
    const {id} = req.params;
        await  casbin.removeResource(String(id));
        res.send({status : true})
});


export const getTodoListShare = tryCatch(async(req:Request,res:Response) => {
    const options = getOptions(req.query);
    const userId = req.user?._id;
        const roles = await casbin.getRoleUser(userId);
    const todos = await getTodosShareService({roles},options);
    res.send(todos)
})