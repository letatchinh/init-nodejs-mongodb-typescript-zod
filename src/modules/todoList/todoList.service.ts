import { Types } from "mongoose";
import { OptionsPaginate } from "../../untils/getOptions";
import TodoList from "./todoList";

export async function createTodo(input:any) {
    try {
        return await TodoList.create(input);
    } catch (error : any) {
        throw new Error(error)
    }
}

export async function getTodosService(query : any = {},options:OptionsPaginate) {
    try {
        return await TodoList.aggregatePaginate(TodoList.aggregate()
          .addFields({todoSeqNum: {$toInt : '$todoSeq'}})
          .match({todoSeqNum : {$gte : 10002}})
          .project({todoSeqNum : 0})
          .lookup({
            from: 'Tag',
            localField : 'tagIds',
            foreignField : '_id',
            as:'tags'
        })
        ,options)
    } catch (error : any) {
        throw new Error(error)
    }
}
export async function getTodoService(id : any) {
    try {
        const todo = await TodoList.findById(id);
        return todo
    } catch (error : any) {
        throw new Error(error)
    }
}
export async function updateTodoService(id : any,params : any) {
    try {
        const todo = await TodoList.findByIdAndUpdate(id,params);
        return todo
    } catch (error : any) {
        throw new Error(error)
    }
}
export async function deleteTodoService(id : any) {
    try {
        const todo = await TodoList.findByIdAndDelete(id);
        return todo
    } catch (error : any) {
        throw new Error(error)
    }
}

export async function getTodosShareService(query : any = {},options:OptionsPaginate) {
    try {
        const {roles} = query;
        let rolesArray:any = [];
        const todoIds = roles?.map((item:string) => {
            const splitRole = item.split(':');
            const role = splitRole[0];
            const resource = splitRole[1];
            rolesArray.push({id:resource,role})
            return new Types.ObjectId(resource)
        });
        return await TodoList.aggregatePaginate(TodoList.aggregate()
          .match({_id : {$in : todoIds}})
          .addFields({
            idth: { $toString: '$_id' },
            role: rolesArray // Ass
        })
        .addFields({
            role : {
                $arrayElemAt: [{
                    $filter : {
                        input : '$role',
                        as : 'item',
                        cond: {
                            $eq :['$$item.id','$idth']
                        },
                        limit : 1
                    }
                },0]
            },
        })
        .addFields({
            role : '$role.role'
        })
        ,options)
    } catch (error : any) {
        throw new Error(error)
    }
}