import { Schema, Types } from "mongoose";
import { OptionsPaginate } from "../../untils/getOptions";
import { getAllUsersCreateNotificationService } from "../user/user.service";
import NotificationUser from "./notificationUser";

export async function createNotificationUserService(input:any) {
    try {
        return await NotificationUser.create(input);
    } catch (error : any) {
        throw new Error(error)
    }
}
export async function insertManyNotificationUserService(query:any,notificationId:Schema.Types.ObjectId) {
    try {
        const users = await getAllUsersCreateNotificationService(query,notificationId);
        return await NotificationUser.insertMany(users);
    } catch (error : any) {
        throw new Error(error)
    }
}

export async function getNotificationUsersService(query : any = {},options:OptionsPaginate) {
    try {
        return await NotificationUser.aggregatePaginate(NotificationUser.aggregate()
          .addFields({todoSeqNum: {$toInt : '$todoSeq'}})
          .match({todoSeqNum : {$gte : 10002}})
          .project({todoSeqNum : 0})
          .lookup({
            from: 'Tag',
            localField : 'tagIds',
            foreignField : '_id',
            as:'tags'
        })
        ,options)
    } catch (error : any) {
        throw new Error(error)
    }
}
export async function getNotificationUserService(id : any) {
    try {
        const todo = await NotificationUser.findById(id);
        return todo
    } catch (error : any) {
        throw new Error(error)
    }
}
export async function updateNotificationUserService(id : any,params : any) {
    try {
        const todo = await NotificationUser.findByIdAndUpdate(id,params);
        return todo
    } catch (error : any) {
        throw new Error(error)
    }
}
export async function deleteNotificationUserService(id : any) {
    try {
        const todo = await NotificationUser.findByIdAndDelete(id);
        return todo
    } catch (error : any) {
        throw new Error(error)
    }
}

export async function getNotificationUsersShareService(query : any = {},options:OptionsPaginate) {
    try {
        const {roles} = query;
        let rolesArray:any = [];
        const todoIds = roles?.map((item:string) => {
            const splitRole = item.split(':');
            const role = splitRole[0];
            const resource = splitRole[1];
            rolesArray.push({id:resource,role})
            return new Types.ObjectId(resource)
        });
        return await NotificationUser.aggregatePaginate(NotificationUser.aggregate()
          .match({_id : {$in : todoIds}})
          .addFields({
            idth: { $toString: '$_id' },
            role: rolesArray // Ass
        })
        .addFields({
            role : {
                $arrayElemAt: [{
                    $filter : {
                        input : '$role',
                        as : 'item',
                        cond: {
                            $eq :['$$item.id','$idth']
                        },
                        limit : 1
                    }
                },0]
            },
        })
        .addFields({
            role : '$role.role'
        })
        ,options)
    } catch (error : any) {
        throw new Error(error)
    }
}