import mongoose, { Schema } from "mongoose";
import mongooseAggregatePaginate from "mongoose-aggregate-paginate-v2";
import mongoosePaginate from 'mongoose-paginate-v2';
import CombinedPaginateModel from "../../untils/ComparePaginate";
import { STATUS_NOTIFICATION_USER } from "./defaultValue";

export interface NotificationUserDocument extends mongoose.Document {
    notificationId: Schema.Types.ObjectId;
    userId:Schema.Types.ObjectId;
    status:string;
}


const NotificationUserSchema = new mongoose.Schema({
    notificationId: { type: Schema.Types.ObjectId, require: true , ref : "Notification" },
    userId: { type:  Schema.Types.ObjectId, require: true, ref: "UserSchema" },
    status: { type: String, require: true, enum : Object.values(STATUS_NOTIFICATION_USER),default : STATUS_NOTIFICATION_USER.NEW},
}, {
    timestamps: true,
});
  NotificationUserSchema.plugin(mongoosePaginate);
  NotificationUserSchema.plugin(mongooseAggregatePaginate);
  const NotificationUser = mongoose.model<NotificationUserDocument,CombinedPaginateModel<NotificationUserDocument>>('NotificationUser', NotificationUserSchema,'NotificationUser');
  export default NotificationUser;