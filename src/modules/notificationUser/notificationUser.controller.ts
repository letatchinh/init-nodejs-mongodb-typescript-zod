
import {Request,Response} from 'express'
import casbin from '../../untils/casbin'
import { addRoleUser } from '../../untils/casbin/interface'
import getOptions from '../../untils/getOptions'
import tryCatch from '../../untils/tryCatch'
import { createNotificationUserService, getNotificationUsersService,getNotificationUserService, updateNotificationUserService, deleteNotificationUserService, getNotificationUsersShareService } from './notificationUser.service'
export const createNotificationUser = tryCatch(async(req:Request,res:Response) => {
    const notificationUser = await createNotificationUserService(req.body);
    res.send(notificationUser)
})
export const getNotificationUsers = tryCatch(async(req:Request,res:Response) => {
    const options = getOptions(req.query);
    options.populate = {path : 'userId',select : 'fullName email phoneNumber'}
    const {} = req.query
    const notificationUsers = await getNotificationUsersService({},options);
    
    res.send(notificationUsers)
})
export const getNotificationUser = tryCatch(async(req:Request,res:Response) => {
    const {id} = req.params;
    const notificationUser = await getNotificationUserService(id);
    res.send(notificationUser)
})
export const updateNotificationUser = tryCatch(async(req:Request,res:Response) => {
    const {id} = req.params;
    const notificationUser = await updateNotificationUserService(id,req.body);
    res.send(notificationUser)
})
export const deleteNotificationUser = tryCatch(async(req:Request,res:Response) => {
    const {id} = req.params;
    const notificationUser = await deleteNotificationUserService(id);
    res.send(notificationUser)
});
