import { Express } from 'express';
import Authentication from '../../middleware/Authentication';
import { createNotificationUser, deleteNotificationUser, getNotificationUser, getNotificationUsers, updateNotificationUser } from './notificationUser.controller';
const routes = (app:Express) => {
    app.post("/api/v1/notificationUser",
    Authentication,
    createNotificationUser
    );
    app.get("/api/v1/notificationUser",
    Authentication,
    getNotificationUsers
    );
    app.get("/api/v1/notificationUser/:id",
    Authentication,
    getNotificationUser
    );

    app.put("/api/v1/notificationUser/:id",
    Authentication,
    updateNotificationUser
    );
    app.delete("/api/v1/notificationUser/:id",
    Authentication,
    deleteNotificationUser
    );
}
export default routes