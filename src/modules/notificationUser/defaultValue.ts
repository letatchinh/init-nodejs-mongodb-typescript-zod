
export const STATUS_NOTIFICATION_USER = {
    NEW:"NEW",
    SEEN_BUT_UNREAD:"SEEN_BUT_UNREAD",
    READ:"READ",
}
