import { Express } from 'express'
import Authentication from '../../middleware/Authentication';
import { createTagController, getTagController } from './tag.controller';
const routes = (app:Express) => {
    app.post("/api/v1/tag",
    // validate(TodoListValidate),
    createTagController);
    app.get("/api/v1/tag",
    Authentication,
    getTagController);
}
export default routes