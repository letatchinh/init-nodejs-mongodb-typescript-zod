
import {Request,Response} from 'express'
import getOptions from '../../untils/getOptions'
import tryCatch from '../../untils/tryCatch'
import { createTagService, getTagService } from './tag.service'
export const createTagController = tryCatch(async(req:Request,res:Response) => {
    const newTag = await createTagService(req.body)
    res.send(newTag)
})
export const getTagController = tryCatch(async(req:Request,res:Response) => {
    const options = getOptions(req.query);
    const {} = req.query
    const tags = await getTagService({},options)
    res.send(tags)
})