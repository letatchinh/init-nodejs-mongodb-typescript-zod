import { OptionsPaginate } from "../../untils/getOptions";
import TagCollection from "./tag.model";

export const createTagService = async(input:any) => {
    try {
        return await TagCollection.create(input)
    } catch (error:any) {
        throw new Error(error)
    }
}
export const getTagService = async(query:any,options:OptionsPaginate) => {
    try {
        return await TagCollection.paginate(query,options)
    } catch (error:any) {
        throw new Error(error)
    }
}