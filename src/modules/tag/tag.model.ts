import mongoose, { Schema } from "mongoose";
import paginate from 'mongoose-paginate-v2';

export interface TagDocument extends mongoose.Document {
    name: string;
    color?: string;
}

const TagSchema = new mongoose.Schema({
    name: { type: String, require: true },
    color: { type: String },
}, {
    timestamps: true,
});
  TagSchema.plugin(paginate)
  const TagCollection = mongoose.model<TagDocument,mongoose.PaginateModel<TagDocument>>('Tag', TagSchema,'Tag');
  export default TagCollection;