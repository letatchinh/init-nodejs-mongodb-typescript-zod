import express, { Request, Response } from "express";
import {config} from 'dotenv';
import cors from 'cors';
import './untils/database';
import "./untils/casbin";
import routes from "./untils/routes";
import errorHandle from "./untils/errorHandle";
import push from 'web-push';
import { get } from "lodash";
config();
const validKey = {
    publicKey: 'BJB_zO3TLIyiJr3DueOL21KR0rgmc0bxBAwgtv_3ejUMX9oUABzM4-puQJi-RPNGt76OkY1E7Ra-1CBgOxhDJgc',
    privateKey: 'xiX5VOx5S92RdOnGsHgerYFqpUzBnQyv_7k85QBy8MQ'
  };
  push.setVapidDetails('mailto:test@code.co.uk',validKey.publicKey,validKey.privateKey);

  
const app  = express();
app.use(cors())
app.use(express.json());
const subscriptions:any = [];

app.post('/subscribe', (req:Request, res:Response) => {
  const {subscription,userId} = req.body;
  
  subscriptions.push({subscription,userId});
  res.status(201).json({ message: 'Subscription saved' });
});


app.post('/send-notification', (req, res) => {
    const {message,data} = req.body;
  const notificationPayload = JSON.stringify({ title: 'Push Notification', body: message });
  
    const findSub = subscriptions?.filter((sub:any) => get(sub,'userId') === data.userId);
    
  Promise.all(findSub.map(({subscription}:any) =>
    {        
        return push.sendNotification(subscription, notificationPayload)
    }
  ))
    .then(() => res.status(200).json({ message: 'Notification sent' }))
    .catch(error => {
      console.error('Error sending push notification:', error);
      res.status(500).json({ error: 'Notification failed to send' });
    });
});

routes(app)
const PORT : Number = Number(process.env.PORT) || 4000 
app.use(errorHandle)
app.listen(PORT , async() => console.log("INPORT ",PORT));

