export const CORE_ACTION = {
    read: {
        name: "Đọc",
        value: "read",
    },
    update: {
        name: "Cập nhật",
        value: "update",
    },
    delete: {
        name: "Xoá",
        value: "delete",
    },
    admin: {
        name: "Quản lý",
        value: 'admin',
    }
};

export const GROUP_POLICIES = {
    READONLY: [CORE_ACTION.read.value],
    EDITOR: [CORE_ACTION.read.value, CORE_ACTION.update.value],
    ADMIN: [CORE_ACTION.read.value, CORE_ACTION.update.value, CORE_ACTION.admin.value],
    OWNER: [CORE_ACTION.read.value, CORE_ACTION.update.value, CORE_ACTION.admin.value, CORE_ACTION.delete.value],
}
export enum CORE_POLICIES {
    READONLY = "READONLY",
    EDITOR = "EDITOR",
    ADMIN = "ADMIN",
    OWNER = "OWNER",
}