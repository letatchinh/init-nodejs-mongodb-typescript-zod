import express from 'express';
import {AnyZodObject} from 'zod'
import { InvalidError } from '../untils/ErrorBase';
const validate = (schema : AnyZodObject) => (req : express.Request,res:express.Response,next:express.NextFunction) => {
try {
    
    schema.parse({
        body : req.body,
        query : req.query,
        params : req.params
    })
    next()
} catch (error:any) {
    next(new InvalidError())
}
}
export default validate