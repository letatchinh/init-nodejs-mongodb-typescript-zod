import { Response, NextFunction, Request } from 'express'
import casbin from '../untils/casbin';
import { AuthorizationError } from '../untils/ErrorBase';
import verifyToken from './verifyToken';
const Authorization = (policies: any) => {
  return async (req: Request, res: Response, next: NextFunction) => {
    try {
      verifyToken(req);
      const userId = req.user._id;
      const resource = req.headers.resource;
      if (!resource) {
        throw new AuthorizationError("Need resource In header");
      }
      const promises: Promise<any>[] = policies?.map((policy: any) => casbin.enforcer?.enforce(userId, resource, policy));
      await Promise.allSettled(promises).then((results: Array<{ status: 'fulfilled'; value: any } | { status: 'rejected'; reason: any }>) => {
        const isHasPermission = results.some((result) => {
          if (result.status === 'fulfilled') {
            return result.value
          }
        });
        if (isHasPermission) {
          next();
        } else {
          throw new AuthorizationError();
        }

      })

    } catch (error: any) {
      next(error)
    }
  }
};
export default Authorization