import { AuthenticateError } from "../untils/ErrorBase";
import jwt from 'jsonwebtoken';
import { Request } from "express";
const verifyToken = (req:Request) => {
    const Authentication:string | undefined = req.headers.authorization;

        if(!Authentication) {
            throw new AuthenticateError();
        }
        const token = Authentication.split(' ')[1];
        if(!token){
            throw new AuthenticateError();
        };
        jwt.verify(token,process.env.JWT_SECRET || "",(err:any,data:any) => {
            if(err){
                throw new AuthenticateError();
            }else{
                req.user = data;
            }
        });
}
export default verifyToken;