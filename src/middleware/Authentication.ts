import {Request,Response,NextFunction} from 'express'
import { AuthenticateError } from '../untils/ErrorBase';
import jwt from 'jsonwebtoken'
import verifyToken from './verifyToken';
const Authentication = (req:Request,res:Response,next:NextFunction) => {
    try {
        verifyToken(req);
        next();
    } catch (error) {
        next(error);
    }
    
}
export default Authentication