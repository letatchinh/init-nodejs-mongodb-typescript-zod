import { array, boolean, never, object, string, TypeOf } from "zod";

export const TodoListValidate = object({
    body : object({
        title: string({
            required_error : "Tiêu đề cần phải nhập!"
        }),
        desc: string(),
        isImportant: boolean().optional(),
        // todoId : never()
    })
})
// export type createTodoListInput = TypeOf<typeof TodoListValidate>;
const roleItemSchema = object({
    userId: string(),
    role: string(),
    todoId: string(),
  });
  const headerSchema = object({
    // Define the properties you expect in the header
    // For example, you might want to validate an authentication token
    authToken: string().refine(value => value.length > 0, {
        message: 'resource is required.',
    }),
    resource: string().refine(value => value.length > 0, {
        message: 'resource is required.',
    }),
    
    // Add more properties as needed
});
export const addRolesTodoListValidate = object({
    body : array(roleItemSchema),
});
export const removeRolesTodoListValidate = object({
    body : object({
        userId: string(),
        todoId: string(),
    }),
});
// export type addRolesTodoListInput = TypeOf<typeof addRolesTodoListValidate>