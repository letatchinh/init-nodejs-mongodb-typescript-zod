import bcrypt from 'bcrypt'
const saltRounds = 10;

export const handleHash = (data:string) => {
    const salt = bcrypt.genSaltSync(saltRounds);
    const dataHash = bcrypt.hashSync(data, salt);
    return dataHash
}
export const handleCompare = (dataInput:string,target:string) => {
    const isEqual = bcrypt.compareSync(dataInput, target); // true
    return isEqual
}