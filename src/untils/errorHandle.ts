import {Request,Response,NextFunction}from 'express'
import { ErrorBase } from './ErrorBase'
const errorHandle = (error:any,req:Request,res:Response,next:NextFunction) => {    
    if(error instanceof ErrorBase){
        const {code} = error
        return res.status(code || 500).send(error)
    }
    const {code,message} = new ErrorBase()
     return res.status(code || 500).send(message || "Internal Server Error")
}
export default errorHandle