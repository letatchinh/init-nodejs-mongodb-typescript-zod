import mongoose from "mongoose";
import autoIncrementModelID from "../modules/counter/counter";
 export interface optionsAutoIncrement {
  modelName: string,
  inc_field: string,
  start_seq?: number,
  prefix?:string,
}
 function AutoIncrement(schema:mongoose.Schema,options:optionsAutoIncrement) {
    schema.pre('save', async function (this, next:any) {
        if (!this.isNew) {
          next();
          return;
        }
        try {
          await autoIncrementModelID({ doc: this,next,...options});
          next();
        } catch (error:any) {
          next(error);
        }
      });
}
export default AutoIncrement