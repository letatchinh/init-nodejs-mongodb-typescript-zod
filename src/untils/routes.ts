import express from "express";
import userRoutes from '../modules/user/user.routes'
import tagRoutes from '../modules/tag/tag.routes'
import todoListRoutes from '../modules/todoList/todoList.routes'
import notificationRoutes from '../modules/notification/notification.routes';
function routes(app : express.Express){
    todoListRoutes(app);
    userRoutes(app);
    tagRoutes(app);
    notificationRoutes(app);

}
export default routes;