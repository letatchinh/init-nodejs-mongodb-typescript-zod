import { Enforcer, newEnforcer } from "casbin";
import MongooseAdapter from "casbin-mongoose-adapter";
import { forIn, keys } from "lodash";
import path from "path";
import { CORE_POLICIES, GROUP_POLICIES } from "../../core/permission";

class CasBin {
    static instance: CasBin;
    enforcer?: Enforcer;
    constructor() {
        this.init().then(async() => {
            console.log("Connected to CasBin");
        });
    }
    async init() {
        const modelEnforcer = path.resolve(__dirname, './basic_model.conf');
        const adapter = await MongooseAdapter.newAdapter("mongodb://localhost:27017/TodoList");
        this.enforcer = await newEnforcer(modelEnforcer, adapter);
        this.enforcer.initWithAdapter(modelEnforcer,adapter);
    }
    static getInstance() {     
        
        if (!CasBin.instance) {
            CasBin.instance = new CasBin();
        }

        return CasBin.instance;
    }
    combinePolicyAndTodoId(policy: string,todoId: string){
        return `${policy}:${todoId}`
    }
    async addPolicy(data:any) {
        const [policy,todoId,role] = data
        if (this.enforcer) {
        await this.enforcer.addPolicy(policy, todoId,role);
        // Load the policy in store after AddPolicy
        // await this.enforcer.loadPolicy();
            
        } else {
            throw new Error('Enforcer is not initialized.');
        }
    }
    async getRoleUser(userId:string) {
        if (this.enforcer) {
        const roles = await this.enforcer.getRolesForUser(userId);
        return roles
        } else {
            throw new Error('Enforcer is not initialized.');
        }
    }
    async addRoleForUser(userId:string,policy:string,todoId:string) {
        if (this.enforcer) {
            if(policy !== CORE_POLICIES.OWNER){
                await this.removeRoleForUser(userId,todoId);
                await this.enforcer.addRoleForUser(userId,this.combinePolicyAndTodoId(policy,todoId));
            }
        } else {
            throw new Error('Enforcer is not initialized.');
        }
    };
    async removeRoleForUser(userId:string,todoId:string) {
        if (this.enforcer) {
            const roles = await this.getRoleUser(userId);
            const findRolesOfUser = roles?.find(role => role.split(':')[1] === todoId)
            if(findRolesOfUser){
                await this.enforcer.deleteRoleForUser(userId,findRolesOfUser);
            }
        
        } else {
            throw new Error('Enforcer is not initialized.');
        }
    };
    getGroupPolicy(todoId:string){
        let rules:any = [];
        forIn(GROUP_POLICIES,(value,key) => {
            value.map((permission:any) => rules.push([this.combinePolicyAndTodoId(key,todoId),todoId,permission]));
        });
        return rules;
    }
    /**
     * Create a new group policy After Create Todi
     * @param todoId 
     */
    async createGroupPolicyTodoList(todoId:string){
        const rules = this.getGroupPolicy(todoId);
        if(this.enforcer){
            const promises = rules?.map((rule:any) => this.addPolicy(rule))
            await Promise.all(promises);
            await this.enforcer?.loadPolicy();
        }
    };
    async removeResource(todoId:string){
        if (this.enforcer) {
            // Remove the resource from the enforcer
            // Must Remove All policy and Group roles
            const promises = keys(GROUP_POLICIES)?.map((key:any) => this.enforcer?.removeFilteredGroupingPolicy(1, this.combinePolicyAndTodoId(key,todoId))); // G OK
            const rules = this.getGroupPolicy(todoId);
            const promiseRemovePolicy = rules?.map((rule:any) => this.enforcer?.removePolicy(...rule)) // P OK
            await Promise.all(promises);// G
            await Promise.all(promiseRemovePolicy);// P
        } else {
            throw new Error('Enforcer is not initialized.');
        }
        

    };

}

export default CasBin.getInstance();