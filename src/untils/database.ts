import mongoose from "mongoose"


const connectionDatabase =()=>{
    mongoose.connect('mongodb://localhost:27017/TodoList',{} ).then(()=>{
        console.log('connecting to database')
    }).catch((r)=>{
        console.error('Error connecting to database')
        setTimeout(() => {
            connectionDatabase()
        }, 2000);
    })
}

connectionDatabase()