export interface OptionsPaginate {
    page? : number,
    limit? : number,
    lean?:boolean,
    sort?:string |object,
    populate?:any
}
const getOptions = (query:any):OptionsPaginate => {
    const {
        page=1,
        limit=10,
        lean=false,
        sort,
        populate
    } = query
    return {page,limit,lean,sort,populate}
}
export default getOptions