import mongoose, { Aggregate, PaginateOptions } from "mongoose";

interface AggregatePaginateResult<T> {
    docs: T[];
    totalDocs: number;
    limit: number;
    page?: number;
    totalPages: number;
    nextPage?: number | null;
    prevPage?: number | null;
    pagingCounter: number;
    hasPrevPage: boolean;
    hasNextPage: boolean;
    meta?: any;
    [customLabel: string]: T[] | number | boolean | null | undefined;
}
interface CombinedPaginateModel<T extends mongoose.Document> extends  mongoose.PaginateModel<T> {
    aggregatePaginate(
        query?: Aggregate<T[]>,
        options?: PaginateOptions,
        callback?: (err: any, result: AggregatePaginateResult<T>) => void,
    ): Promise<AggregatePaginateResult<T>>;

  }
  export default CombinedPaginateModel