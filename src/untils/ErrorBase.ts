export class ErrorBase extends Error{
    code:number;
    constructor(message?:string,code?:number) {
        super()
        this.code = code || 500;
        this.message = message || "Có lỗi gì đó xảy ra!";
    }
}


export class InvalidError extends ErrorBase{
    constructor(message?:string,code?:number) {
        super()
        this.code = code || 400;
        this.message = message || "Sai định dạng";
    }
}
export class NotFoundError extends ErrorBase{
    constructor(message?:string,code?:number) {
        super()
        this.code = code || 404;
        this.message = message || "Không tìm thấy dữ liệu";
    }
}
export class AuthenticateError extends ErrorBase{
    constructor(message?:string,code?:number) {
        super()
        this.code = code || 403;
        this.message = message || "Bạn cần có token";
    }
}
export class AuthorizationError extends ErrorBase{
    constructor(message?:string,code?:number) {
        super()
        this.code = code || 401;
        this.message = message || "Bạn không có quyền";
    }
}