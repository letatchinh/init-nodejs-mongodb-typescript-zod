import jwt from 'jsonwebtoken'
import { AuthenticateError } from './ErrorBase'

export const signToken = (payload:any,expiresIn = "7d") => {
    const { JWT_SECRET = "" } = process.env
    const accessToken = jwt.sign({ payload }, JWT_SECRET, { expiresIn});
    return accessToken
}
export const verifyTokenService = (token: string) => {
    jwt.verify(token, process.env.JWT_SECRET || "", (err, data: any) => {
        if (err) {
            throw new AuthenticateError()
        } else {
            const dataSendClient = { ...data, token }
            return dataSendClient
        }

    })
}