import { Request, Response } from 'express';
import { get } from 'lodash';
import web_push from 'web-push';
import { createWebPushService, deleteWebPushService, getWebPushExistsService } from '../../modules/webPush/webPush.service';
import tryCatch from '../tryCatch';


class WebPush {
    static instance: WebPush;
    publicKey?:string;
    privateKey?:string;
    constructor() {
        this.publicKey = process.env.PUBLICKEY_WEBPUSH || "";
        this.privateKey = process.env.PRIVATEKEY_WEBPUSH || "";
        this.init().then(async() => {
            console.log("Connected to WebPush");
        });
    }
    async init() {
          WebPush
          web_push.setVapidDetails('letatchinh123@gmail.com',this.publicKey || "",this.privateKey || "");
    }
    static getInstance() {     
        if (!WebPush.instance) {
            WebPush.instance = new WebPush();
        }
        return WebPush.instance;
    }

    getWebPushKeyController = tryCatch(async(req:Request,res:Response) => {
        res.send(process.env.PUBLICKEY_WEBPUSH);
    });

    subscribeController = tryCatch(async(req:Request,res:Response) => {
        const {subscription,userId} = req.body;
        const subscriptionExist = await getWebPushExistsService({endpoint : get(subscription,'endpoint'),userId});
        if(subscriptionExist){
            deleteWebPushService(get(subscriptionExist,'_id'));
        }
        await createWebPushService({subscription,userId});
        res.status(201).json({ message: 'Subscription saved' });
    });

    pushNotification = tryCatch(async(req:Request,res:Response) => {
        const {title} = req.body;

    });

}

export default WebPush.getInstance();