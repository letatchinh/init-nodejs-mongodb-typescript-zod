Các công nghệ đã dùng 
•	Express
•	TypeScript
•	Mongoose Paginate, Mongoose aggregate paginate
•	JWT để Authenticate
•	Casbin để Authorization , Sử dụng mô hình RBAC kết hợp ABAC (Phân theo Role:IdTodo);
•	Bcrypt Mã hoá mật khẩu
•	Mongoose Sequence (Tự custom);
•	Database : mongodb

Quy trình Phân quyền
- Tạo Bài viết đồng thời tạo ra policies cho bài viết đó , và trao quyền owner cho người tạo (√)
- Trao quyền cho người khác (√)
- Cập nhật quyền cho người khác (√)
- Gỡ quyền cho người khác (√)
- Nhượng quyền cho người khác
- Xoá bài viết => xoá quyền (√)
- Find All Todo With UserId and Paginate and Roles of Todo (√)